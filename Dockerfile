FROM php:7.4.2-apache

ADD config/php.ini /usr/local/etc/php/php.ini
RUN  apt-get update && apt-get install -y \
  zlib1g-dev \
  libjpeg-dev \
  libpng-dev \
  libc-client-dev \
  libkrb5-dev \
  libldap2-dev \
  libxml2-dev \
  vim \
  cron \
  libzip-dev \
  --no-install-recommends && rm -r /var/lib/apt/lists/*

RUN PHP_OPENSSL=yes docker-php-ext-configure imap --with-kerberos --with-imap-ssl && \
  docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ && \
  docker-php-ext-install ldap mysqli zip gd imap dom
